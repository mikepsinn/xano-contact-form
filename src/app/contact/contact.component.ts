import { Component, OnInit, ViewChild } from '@angular/core';
import { ApiService } from '../api.service';
import { ConfigService } from '../config.service';
import { ToastService } from '../toast.service';
import * as _ from 'lodash';
import { NgModel, Validators } from '@angular/forms';
import { FormService } from '../form.service';
import { faSpinner } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.scss']
})
export class ContactComponent implements OnInit {
  form;
  domain;
  success;
  loading;
  saving: boolean;
  leads = [];
  activeTab = "submit";

  spinner = faSpinner;

  constructor(
    private api: ApiService,
    private toast: ToastService,
    protected formService: FormService,
    public config: ConfigService
  ) { }

  ngOnInit(): void {
    this.form = this.createForm();
    this.domain = document.location.hostname;
  }

  setActiveTab(tab) {
    this.activeTab = tab;

    switch(tab) {
      case "list":
        this.getLeadData();
        break;
    }
  }

  getLeadData() {
    this.api.get({
      endpoint: `${this.config.xanoApiUrl}/lead`,
      params: {
      },
      stateFunc: state => this.loading = state
    })
    .subscribe((response:any) => {
      this.leads = response;
    }, (err) => {
      this.toast.error(_.get(err, "error.message") || "An unknown error has occured.");
    });
  }

  createForm() {
    return this.formService.createFormGroup({
      obj: {
        name: ['text', [Validators.required]],
        email: ['email', [Validators.required, Validators.email]],
        company: ['text', []],
        request: ['text', [Validators.required]],
      }
    });
  }

  submit() {
    if (!this.form.trySubmit()) return;

    this.api.post({
      endpoint: `${this.config.xanoApiUrl}/lead`,
      params: {
        ...this.form.value
      },
      stateFunc: state => this.saving = state
    })
    .subscribe((response:any) => {
      this.toast.success("Success");
      this.success = true;
      this.setActiveTab("list");
    }, (err) => {
      this.toast.error(_.get(err, "error.message") || "An unknown error has occured.");
    });
  }
}
