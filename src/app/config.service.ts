import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ConfigService {
  title = "Contact Form";
  summary = "This extension provides a fully functioning demo to accept leads from a contact form and to view them.";
  editText = "Get source code";
  editLink = "https://gitlab.com/xano-marketplace/xano-contact-form";
  descriptionHtml = `
  <h4>Description</h4>
  <p>This demo consists of following components:</p>
  <h5>Lead Submission</h5>
  <p>This is your standard contact form that allows you to capture information from a user.</p>
  <h5>Lead List</h5>
  <p>This contacts all the leads you have received through the contact form. This is just a simple view to see the information and does not include any actionable functionality.</p>
  <h5>Additional Customization</h5>
  <p>Once you have this extension installed and have confirmed it is working, you can begin customizing it with your own requirements. Some of these next steps may include the following: adding <a href="https://xano-google-recaptcha.stackblitz.io/" target="_target">Google reCAPTCHA</a> to prevent spam, adding <a href="https://xano-sendgrid-email.stackblitz.io/" target="_target">Sendgrid Email</a> to send yourself an email when a lead is submitted, etc.
  `;
  logoHtml = '';

  requiredApiPaths = [
    '/lead'
  ];

  xanoApiUrl;

  constructor() { }

  isConfigured() {
    return !!this.xanoApiUrl;
  }
}
